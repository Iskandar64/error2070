package mx.itesm.error2070;



import com.badlogic.gdx.graphics.Texture;

/**
 * Created by Aldo on 29/10/2017.
 */

class Escudo extends Objeto{

    private RobotRunner mario;

    public Escudo(Texture textura, float x, float y, RobotRunner mario){
        super(textura, x, y);//Recibe imagen
        this.mario = mario;
    }

    public void actualizarMovimiento(){
        sprite.setY(mario.sprite.getY());
    }

    public boolean chocaCon(Gorath gorath){
        return sprite.getBoundingRectangle().overlaps(gorath.sprite.getBoundingRectangle());
    }

    public boolean chocaCon(Nave nave){
        return sprite.getBoundingRectangle().overlaps(nave.sprite.getBoundingRectangle());
    }

}
