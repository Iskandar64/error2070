package mx.itesm.error2070;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;

/**
 * Created by Daniel Alillo on 04/11/2017.
 */

public class Error extends Game
{
    private final AssetManager assetManager;

    public float shootLoss = 0.75f;
    public float shieldLoss = 0.035f;
    public float scoreFinal = 0;
    public int lastPlayed = 0;
    public float danio = 5;
    public float redSkin = 0;

    public Error() {
        assetManager = new AssetManager();
    }
    Music music;
    Sound mennus;
    @Override
    public void create () {
        // Lo preparamos para que cargue mapas
        assetManager.setLoader(TiledMap.class, new TmxMapLoader(new InternalFileHandleResolver()));
        // Pone la pantalla inicial (Splash)
        setScreen(new PantallaInicial(this));
    }

    // Para que las otras pantallas usen el assetManager
    public AssetManager getAssetManager() {
        return assetManager;
    }

    public void setShootLoss(float shootLoss)
    {
        this.shootLoss = shootLoss;
    }

    @Override
    public void dispose() {
        super.dispose();
        assetManager.clear();
    }
}
