package mx.itesm.error2070;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * SplashScreen. Muestra el logo del Tec por 2 segundos
 */

class PantallaInicial extends Pantalla
{
    private float tiempoVisible = 3.6f;
    private float count = 0;

    // Es la referencia a la aplicación (la única que puede cambiar pantallas)
    private Error juego;

    // Logo del tec
    private Texture texturaLogo;
    private Sprite spriteLogo;
    private Sprite sp;
    private Texture txt;
    private Texto campus;



    // Constructor, guarda la referencia al juego
    public PantallaInicial(Error error)
    {
        this.juego = error;

    }

    //"inicial/logo.png"

    @Override
    public void show() {
        texturaLogo = new Texture(Gdx.files.internal("Fondos/azulTec.jpg"));
        //txt = new Texture(Gdx.files.internal("tgc.jpg"));
        spriteLogo = new Sprite(texturaLogo);
        //sp = new Sprite(txt);
        spriteLogo.setPosition(ANCHO/2-spriteLogo.getWidth()/2, ALTO/2-spriteLogo.getHeight()/2);
        escalarLogo();

        campus = new Texto("galaxy.fnt", true);
    }

    // hace que mantenga la proporción ancho-alto. (SOLO en landscape)
    private void escalarLogo() {
        float factorCamara = ANCHO / ALTO;
        float factorPantalla = 1.0f*Gdx.graphics.getWidth() / Gdx.graphics.getHeight();
        float escala = factorCamara / factorPantalla;
        spriteLogo.setScale(escala, 1);
    }

    @Override
    public void render(float delta) {

        // Dibujar
        count += delta;
        borrarPantalla(1,1,1);

        batch.setProjectionMatrix(camara.combined);
        batch.begin();
        // Dibuja el logo centrado
        /*if(count < 2.5)
        {
            spriteLogo.draw(batch);
        }
        else
        {
            sp.draw(batch);
        }*/
        spriteLogo.draw(batch);
        campus.mostrarMensaje(batch,"Campus Estado de Mexico",ANCHO/2,100);
        batch.end();

        // Actualizar para cambiar pantalla
        tiempoVisible -= delta;
        if (tiempoVisible<=0) { // Se acabaron los dos segundos

            juego.setScreen(new PantallaCargando(juego, Pantallas.MENU));
        }
    }

    @Override
    public void actualizarVista() {
        escalarLogo();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        // Libera las texturas
        texturaLogo.dispose();
    }
}
