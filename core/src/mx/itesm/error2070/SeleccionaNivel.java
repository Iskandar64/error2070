package mx.itesm.error2070;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.FillViewport;

/**
 * Created by Daniel Alillo on 12/10/2017.
 *
 * Aqui se elegirá si se quiere jugar la campaña o ver el tutorial primero
 */


public class SeleccionaNivel extends ScreenAdapter
{
    private final Error game;
    private static final float WIDTH = 1280;
    private static final float HEIGHT = 720;
    private Stage stage;

    private Table table;//tabla para colocar los botones
    private Texture cityAttack;//cuando se  elige ese nivel
    private Texture cityAttackPress;//
    private Texture retribution;//boton que lleva a ese nivel
    private Texture retributionPress;
    private Texture holdthem;
    private Texture holdthemPress;

    private Texture backgroundTexture;
    private Texture titleTexture;

    private Texture atras;//para regrsar al menuprincipal
    private Texture atrasPress;//cuando presionemos atras
    private ImageButton atBtn;

    public SeleccionaNivel(Error game)
    {
        this.game = game;
    }

    @Override
    public void show()
    {
        super.show();
        stage = new Stage(new FillViewport(WIDTH, HEIGHT));
        Gdx.input.setInputProcessor(stage);

        backgroundTexture = new Texture(Gdx.files.internal("Fondos/Fondo3.jpg"));
        Image background = new Image((backgroundTexture));
        stage.addActor(background);

        cityAttack = new Texture(Gdx.files.internal("botones/attack.png"));
        cityAttackPress =  new Texture(Gdx.files.internal("botones/attack1.png"));

        ImageButton cityBtn = new ImageButton(new TextureRegionDrawable(new TextureRegion(cityAttack)),
                new TextureRegionDrawable((new TextureRegion(cityAttackPress))));
        cityBtn.addListener(new ActorGestureListener() {

            public void tap(InputEvent event, float x, float y, int count, int button)
            {
                super.tap( event,  x,  y,  count, button);
                game.music.pause();
                game.mennus.play();
                game.setScreen(new PantallaCargando(game, Pantallas.NIVEL_RUNNER));
                dispose();
            }
        });

        holdthem = new Texture(Gdx.files.internal("botones/holdthem.png"));
        holdthemPress =  new Texture(Gdx.files.internal("holdThem1.png"));

        ImageButton holdBtn = new ImageButton(new TextureRegionDrawable(new TextureRegion(holdthem)),
                new TextureRegionDrawable((new TextureRegion(holdthemPress))));
        holdBtn.addListener(new ActorGestureListener() {

            public void tap(InputEvent event, float x, float y, int count, int button)
            {
                super.tap( event,  x,  y,  count, button);
                game.music.pause();
                game.mennus.play();
                game.setScreen(new PantallaCargando(game, Pantallas.NIVEL_COLLECT));
                dispose();
            }
        });

        retribution = new Texture(Gdx.files.internal("botones/retribution.png"));
        retributionPress =  new Texture(Gdx.files.internal("botones/retribution1.png"));

        ImageButton retBtn = new ImageButton(new TextureRegionDrawable(new TextureRegion(retribution)),
                new TextureRegionDrawable((new TextureRegion(retributionPress))));
        retBtn.addListener(new ActorGestureListener() {

            public void tap(InputEvent event, float x, float y, int count, int button)
            {
                super.tap( event,  x,  y,  count, button);
                game.mennus.play();
                game.music.pause();
                game.setScreen(new PantallaCargando(game, Pantallas.NIVEL_DOOM));
                dispose();
            }
        });



        atras = new Texture(Gdx.files.internal("botones/botonMenu.png"));
        atrasPress =  new Texture(Gdx.files.internal("botones/botonMenu1.png"));

        atBtn = new ImageButton(new TextureRegionDrawable(new TextureRegion(atras)),
                new TextureRegionDrawable((new TextureRegion(atrasPress))));
        atBtn.addListener(new ActorGestureListener() {

            public void tap(InputEvent event, float x, float y, int count, int button)
            {
                super.tap( event,  x,  y,  count, button);
                game.setScreen(new MenuPrincipal(game));
                dispose();
            }
        });
        atBtn.setPosition(900,30);
        titleTexture = new Texture(Gdx.files.internal("botones/niveles.png"));
        Image title =  new Image(titleTexture);


        table = new Table();
        table.add(title).padTop(50f).colspan(3).expand();
        table.row();//siguiente fila
        table.add(cityBtn).padBottom(25f).colspan(1).expandY().expandX().uniform();
        table.row();
        table.add(holdBtn).padBottom(25f).colspan(1).expandY().expandX().uniform();
        table.row();
        table.add(retBtn).padBottom(25f).colspan(1).expandY().expandX().uniform();


        table.setFillParent(true);
        table.pack();

        stage.addActor(table);
        stage.addActor(atBtn);

    }



    @Override
    public void render(float delta)
    {
        super.render(delta);
        clearScreen();
        stage.act(delta);
        stage.draw();


    }

    @Override
    public void resize(int width, int height)
    {
        super.resize(width, height);
        stage.getViewport().update(width, height);

    }


    private void clearScreen()
    {
        Gdx.gl.glClearColor((float)0, (float)0, 0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }


    public void dispose()
    {
        super.dispose();;
        stage.dispose();
        retribution.dispose();
        retributionPress.dispose();
        holdthemPress.dispose();
        holdthem.dispose();
        cityAttackPress.dispose();
        cityAttack.dispose();
        backgroundTexture.dispose();
        atras.dispose();
        atrasPress.dispose();
    }

}
