package mx.itesm.error2070;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.FillViewport;

/**
 * Created by Daniel Alillo on 12/10/2017.
 */

public class Creditos extends ScreenAdapter
{
    private final Error game;
    private static final float WORDL_WIDTH = 1280;
    private static final float WORDL_HEIGHT = 720;

    private Table table;//tabla para colocar los botones

    private Texture atras;//para regrsar al menuprincipal
    private Texture atrasPress;//cuando presionemos atras
    private Texture backgroundTexture;


    private Stage stage;

    public Creditos(Error game)
    {

        this.game = game;
    }


    @Override
    public void show() {
        super.show();
        stage = new Stage(new FillViewport(WORDL_WIDTH, WORDL_HEIGHT));
        Gdx.input.setInputProcessor(stage);

        backgroundTexture = new Texture(Gdx.files.internal("creditos.jpg"));//nombres de los integrantes
        Image background = new Image((backgroundTexture));
        stage.addActor(background);

        atras = new Texture(Gdx.files.internal("botones/botonMenu.png"));
        atrasPress =  new Texture(Gdx.files.internal("botones/botonMenu1.png"));

        ImageButton atBtn = new ImageButton(new TextureRegionDrawable(new TextureRegion(atras)),
                new TextureRegionDrawable((new TextureRegion(atrasPress))));
        atBtn.addListener(new ActorGestureListener() {

            public void tap(InputEvent event, float x, float y, int count, int button)
            {
                super.tap( event,  x,  y,  count, button);
                game.setScreen(new MenuPrincipal(game));
                dispose();
            }
        });

       atBtn.setPosition(800, 50);

        stage.addActor(atBtn);
    }


    @Override
    public void render(float delta)
    {
        super.render(delta);
        clearScreen();
        stage.act(delta);
        stage.draw();

    }

    @Override
    public void resize(int width, int height)
    {
        super.resize(width, height);
        stage.getViewport().update(width, height);

    }


    private void clearScreen()
    {
        Gdx.gl.glClearColor((float)0, (float)0, 0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    public void dispose()
    {
        super.dispose();;
        stage.dispose();
        atras.dispose();
        backgroundTexture.dispose();
        atrasPress.dispose();

    }
}
