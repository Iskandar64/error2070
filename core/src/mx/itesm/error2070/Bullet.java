package mx.itesm.error2070;

import com.badlogic.gdx.graphics.Texture;

/**
 * Created by Daniel Alillo on 01/11/2017.
 */

public class Bullet extends Objeto
{
    private final float VELOCIDAD_X = -650;      // Velocidad horizontal (a la derecha)

    // Recibe la imagen
    public Bullet(Texture textura, float x, float y) {
        super(textura, x, y);
    }

    // Mueve el personaje a la derecha
    public void mover(float delta) {
        float distancia = VELOCIDAD_X*delta;
        sprite.setX(sprite.getX()+distancia);
    }

    public boolean chocaCon(RobotRunner robotRunner) {
        return sprite.getBoundingRectangle().overlaps(robotRunner.sprite.getBoundingRectangle());
    }

    public boolean chocaCon(Escudo escudo) {
        return sprite.getBoundingRectangle().overlaps(escudo.sprite.getBoundingRectangle());
    }
}

