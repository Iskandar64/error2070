package mx.itesm.error2070;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Fondo continuo, ancho mayor que la cámara
 */

class Fondo
{
    private Texture textura;
    private float x;
    private  float velocidad ;    // pixeles/seg.


    public Fondo(Texture textura, float vel) {
        this.textura = textura;
        velocidad = vel;
        x=0;
    }

    public float getX() {
        return x;
    }

    public void dibujar(SpriteBatch batch, float delta, EstadoJuego state) {
        // Dibujar en posición actual
        batch.draw(textura, x, 0);    // Primer fondo
        if(state==EstadoJuego.JUGANDO)
        {
            float xDerecha = x + textura.getWidth();
            if ( xDerecha>=0 && xDerecha<Pantalla.ANCHO) {
                batch.draw(textura, xDerecha, 0);   // Segundo fondo
            }
            // Nueva posición
            x -= velocidad*delta;
            if (x <= -textura.getWidth()) { // Se sale de la pantalla?
                x = x + textura.getWidth();
            }
        }

    }
}
