package mx.itesm.error2070;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Daniel Alillo on 29/10/2017.
 */

public class Gorath extends Objeto
{

    private  float VELOCIDAD_X ;      // Velocidad horizontal (a la izquierda)

    private Animation<TextureRegion> spriteAnimado;
    private float timerAnimacion;

    private Animation<TextureRegion> explode;


    public Life life;
    public boolean muerto;
    public float timeToDie;

    private float maxHeight;
    private float minHeight;

    private float yIncrease;


    // Recibe una imagen con varios frames (ver enemigo.png)
    public Gorath(Texture textura, Texture explosion, float x, float y, int o, float vel) {
        // Lee la textura como región
        VELOCIDAD_X = vel;

        TextureRegion texturaCompleta = new TextureRegion(textura);
        minHeight = 50;
        maxHeight = 450;
        yIncrease = VELOCIDAD_X/110;
        if(y>250)
        {
            yIncrease = -yIncrease;
        }

        if(o<3)
        {
            yIncrease = 0;
        }

        TextureRegion[][] texturaPersonaje = texturaCompleta.split(158,146);

        spriteAnimado = new Animation(0.1f, texturaPersonaje[0][0], texturaPersonaje[0][1],
                texturaPersonaje[0][2],texturaPersonaje[0][3],texturaPersonaje[0][4]);
        // Animación infinita
        spriteAnimado.setPlayMode(Animation.PlayMode.LOOP);
        // Inicia el timer que contará tiempo para saber qué frame se dibuja
        timerAnimacion = 0;
        // Crea el sprite

        TextureRegion texturaEx = new TextureRegion(explosion);
        TextureRegion[][] texturaExplota = texturaEx.split(182,153);
        explode =  new Animation(0.02f, texturaExplota[0][0], texturaExplota[0][1],
                texturaExplota[0][2],texturaExplota[0][3],texturaExplota[0][4],texturaExplota[0][5]
                ,texturaExplota[0][6],texturaExplota[0][7],texturaExplota[0][8],texturaExplota[0][9]
                ,texturaExplota[0][10],texturaExplota[0][11]);
        explode.setPlayMode(Animation.PlayMode.LOOP);


        sprite = new Sprite(texturaPersonaje[0][0]);
        sprite.setPosition(x,y);    // Posición inicial
        life = Life.ALIVE;
        muerto = false;
        timeToDie = 0;
    }

    // Dibuja el Gorath
    public void dibujar(SpriteBatch batch, EstadoJuego estadoJuego) {

        if(life == Life.ALIVE)
        {
            // Dibuja el personaje dependiendo del estadoMovimiento
            if(estadoJuego == EstadoJuego.JUGANDO)
            {
                timerAnimacion += Gdx.graphics.getDeltaTime();
                // Frame que se dibujará
                TextureRegion region = spriteAnimado.getKeyFrame(timerAnimacion);
                batch.draw(region, sprite.getX(), sprite.getY());
            }
            else
            {
                sprite.draw(batch);
            }
        }
        else
        {
            timeToDie += Gdx.graphics.getDeltaTime();
            //timeExplode += Gdx.graphics.getDeltaTime();

            TextureRegion region = explode.getKeyFrame(timeToDie);
            batch.draw(region, sprite.getX(), sprite.getY());

            if(timeToDie >0.25)
            {
                muerto = true;
            }
        }

    }

    public void kill()
    {
        if(life == Life.ALIVE)
        {
            life = Life.DEAD;
        }
    }



    public void mover(float delta) {
        if(life == Life.ALIVE)
        {
            float distancia = VELOCIDAD_X*delta;
            sprite.setX(sprite.getX()+distancia);
            sprite.setY(sprite.getY()+yIncrease);

            if(sprite.getY()>= maxHeight || sprite.getY() <= minHeight)
            {
                yIncrease = -yIncrease;
            }


        }
    }

    public boolean clash(RobotRunner robotRunner) {
        if(life == Life.ALIVE)
        {
            return sprite.getBoundingRectangle().overlaps(robotRunner.sprite.getBoundingRectangle());
        }
        return false;
    }

    public enum Life
    {
        DEAD,
        ALIVE
    }

}
