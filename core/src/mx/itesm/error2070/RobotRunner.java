package mx.itesm.error2070;

import com.badlogic.gdx.graphics.Texture;

/**
 * Created by Daniel Alillo on 29/10/2017.
 */

public class RobotRunner extends Personaje
{

    // Variables para el salto
    private final float gravity = 98.1f;  // Gravedad
    private final float speed = 205;  // Velocidad de salida (hacia arriba)
    private float ymax;     // Altura máxima
    // Tiempo de vuelo TOTAL

    private float velTemp;
    private float energia;

    private float highness;  // La posición actual cuando está saltando
    private float timeFly;    // El tiempo que ha transcurrido desde que inició el salto
    private float timeFly2;
    private float yInicial;     // Posición donde inicia el salto


    private Personaje.EstadoSalto estadoSalto = Personaje.EstadoSalto.EN_PISO;
    private Boolean doble = false;

    public RobotRunner(Texture textura, float x, float y, int skin, Texture brinco1, Texture brinco2) {
        super(textura, x, y, skin, brinco1, brinco2);

        setEstadoJuego(EstadoJuego.JUGANDO);
        this.energia = 100;
    }

    public void actualizar(float delta) {
        // Calcula la nueva posición (por ahora cuando está saltando)
        if(energia>100)
        {
            energia = 100;
        }
        if ( estadoSalto == EstadoSalto.SALTANDO ) {
            timeFly += delta*5;   // El factor DES/ACELERA
            jump = true;
            if(doble )
            {
                timeFly2 += delta*5;
                dobleSatlo = true;
                highness = velTemp*(timeFly2+timeFly)-0.5f*gravity*timeFly*timeFly;
            }
            else
            {
                highness = speed*timeFly-0.5f*gravity*timeFly*timeFly;
            }


            if (yInicial+highness>yInicial) {
                //Sigue en el aire

                sprite.setY(yInicial+highness);
                sprite1.setY(yInicial+highness);
                sprite2.setY(yInicial+highness);
            } else {
                // Termina el salto
                sprite.setY(yInicial);
                sprite1.setY(yInicial);
                sprite2.setY(yInicial);
                estadoSalto = EstadoSalto.EN_PISO;
                doble = false;
                dobleSatlo = false;
                jump = false;
            }
        }
    }


    public void saltar() {
        if (estadoSalto!=EstadoSalto.SALTANDO && !doble) {    // Si aun no ahces el doble salto
            // Iniciar el salto
            ymax = (speed * speed) / (2 * gravity);

            highness = 0;    // Inicia en el piso
            timeFly = 0;
            timeFly2 = 0;
            yInicial = sprite.getY();
            estadoSalto = EstadoSalto.SALTANDO;


        }
        else if (estadoSalto==EstadoSalto.SALTANDO && !doble) {    // No puedes hacer dos doble saltos


            doble = true;

            if(highness<ymax)
            {
                velTemp = 180;

            }
            else
            {
                velTemp= speed;
            }
        }
    }

    public float getEnergia() {
        return energia;
    }

    public void setEnergia(float energia) {
        this.energia = energia;
    }

    public EstadoSalto getEstadoSalto() {
        return estadoSalto;
    }

}
