package mx.itesm.error2070;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.ArrayList;

/**Clase de Niveles
 * Primer Nivel
 * Hay que sobrevivir el recorrido
 *
 */

class PantallaRunner extends Pantalla
{

 // Para asegurar que hay movimiento
    private final Error juego;
    private final AssetManager manager;
    private float tiempoFire = 6f;

    // Fondo
    private Fondo fondo;
    private Texture texturaFondo;

    private Fondo fondo1;
    private Texture texturaFondo1;

    private Fondo fondo2;
    private Texture texturaFondo2;

    private ShapeRenderer shapeRenderer;

    // Punteros (dedo para paneo horizontal, vertical)
    private final int INACTIVO = -1;
    private int punteroHorizontal = INACTIVO;
    private int punteroVertical = INACTIVO;


    private Music musicaFondo;
    private EstadoSonido estadoSonido;

    private Texture texturaGranada;
    private Array<Granada> granadas;

    private Texture texturaDoom;
    private Doom doom;

    private Texture texturaEscudoDoom;
    private Barrera escudoDoom;
    private ArrayList<Barrera> escudosDoom;

    private Array<BulletDiag> bulletsDiag;
    private Array<Bolt> bolts;
    private Texture boltTexture;

    private RobotRunner robot;

    // Para salir
    private EscenaPausa escenaPausa;

    // Enemigos
    private int gorathCount = 0;
    private Texture texturaGorath;
    private Array<Gorath> enemies;
    private int gorathLimit;//máximo numero de goraths que puede haber en el nivel
    private int enemyQuantity;
    private float enemyWait;
    private boolean gorathHorde;
    private float hordeTime = 0;


    private  Texture texturaExplosion;

    private Texture texturaNave;
    private Array<Nave> naves;
    private int naveLimit;
    private int naveQuantity;
    private float naveWait;


    private Texture pauseButton;
    private Objeto pauseObject;

    private Texture texturaBala;
    private  Array<Bomba> bombas;
    private float bombaTolerance;
    private Array<Bala> balas;
    private float bombWait;
    public EstadoJuego estado = EstadoJuego.JUGANDO;

    private Texto pausaLogo;

    //Escudo
    private Texture texturaEscudo;
    private Array<Escudo> escudos;

    private Array<Bullet> bullets;
    private Texture bulletTexture;

    private long startTime;

    private float enemyTime;
    private long bombTime;

    private Fire fire;
    private Fire fire1;
    private Texture fireTexture;

    private Sound gun;
    private Sound hit;
    private Sound lazer;
    private Sound collect;
    private Sound granadeSound;
    private Sound shieldSound;
    private Sound menu;
    private Sound gorathDeath;

    private Objeto shieldBtn;
    private Texture shieldTexture;
    private Texture shieldPress;
    private Objeto shieldPressBtn;

    private Texture texturabomba;


    private float TimePlaying;

    private  BitmapFont energiaInfo;
    private int score;
    private BitmapFont scoreInfo;

    private int gorathNumber;
    private BitmapFont gorathNO;
    private float altoGorath = 60;

    private Array<AtoCharges> atoCharges;
    private Texture atoTexture;
    private float atoTime;

    private Array<Orb> orbs;
    private Texture orbTexture;
    private float orbTime;

    private Array<Bateria> baterias;
    private Texture bateriaTexture;
    private float bateriaTime;
    private Objeto bateriaLogo;
    private BitmapFont bateriaInfo;

    private Texture texturaRoja;


    private float timeOver;
    private float damage;//unidad basica de danio que se le puede inflingir al robot
    private Nivel level;
    private int potencia;//cuanto danio infligen los disparos a los enemigos
    private float potenciaTime;
    private Power estadoPotencia;

    private Objeto barraVida1;
    private Objeto gorathSymbol;
    private Objeto barraBlack;

    private Texture barraLife;

    private LetreroTitan letreroTitan;
    private boolean titanMode;
    private int bateriaCount = 0;

    private Texture robotTexture;
    private int skinMode;

    private Texture brinco1;
    private Texture brinco2;

    public PantallaRunner(Error juego, int l) {
        super();
        this.juego = juego;
        manager = juego.getAssetManager();
        switch (l)
        {
            case 1:
                level = Nivel.LEVEL1;
                break;
            case(2):
                level = Nivel.LEVEL2;
                break;
            case (3):
                level = Nivel.FINALBOSS;
                break;
        }
    }

    @Override
    public void show() {
        estadoSonido = EstadoSonido.SOUND;

        pausaLogo = new Texto("ethno.fnt", true);

        enemies = new Array<Gorath>();   // Arreglo de goraths
        naves = new Array<Nave>();
        naveLimit = 15;

        bulletsDiag = new Array<BulletDiag>();
        bolts = new Array<Bolt>();

        gun = manager.get("sounds/gun.mp3");
        lazer = manager.get("sounds/disparodes.mp3");
        gorathDeath= manager.get("sounds/neck.mp3");

        collect = manager.get("sounds/explosionDest.mp3");
        granadeSound = manager.get("sounds/granada.mp3");
        menu = manager.get("sounds/menu.mp3");
        shieldSound = manager.get("sounds/escudo.mp3");

        texturaExplosion = manager.get("ex.png");
        granadas = new Array<Granada>();


        shapeRenderer = new ShapeRenderer();


        balas = new Array<Bala>();
        bullets = new Array<Bullet>();
        escudos = new Array<Escudo>(1);
        bombas = new Array<Bomba>();

        potencia =2;
        damage = juego.danio;
        estadoPotencia = Power.OFF;

        gorathHorde = false;
        if(level== Nivel.LEVEL1)
        {
            enemyWait = 5;
            gorathSymbol = new Objeto((Texture)manager.get("gorath1.png"), 245,575);

            texturaFondo1 = manager.get("Fondos/sky.jpg");
            texturaFondo2= manager.get("Fondos/buildings.png");

            fondo1 = new Fondo(texturaFondo1,280);
            fondo2 = new Fondo(texturaFondo2, 350);

            bombWait = 21;
            gorathLimit =7;
            timeOver = 59f;
            atoTime = 20f;
            musicaFondo = manager.get("sounds/Run.mp3");
        }
        else if(level == Nivel.LEVEL2)
        {

            bateriaTexture = manager.get("Collect/bateria.png");
            bateriaTime = 5f;
            bateriaLogo = new Objeto((Texture)manager.get("bateria1.png"), 245,575);

            fireTexture = manager.get("Fondos/FireSprite.png");
            texturaFondo1 = manager.get("Fondos/FondoN2.jpg");
            texturaFondo2= manager.get("Fondos/FondoN2ed.png");

            fondo1 = new Fondo(texturaFondo1,275);
            fondo2 = new Fondo(texturaFondo2, 350);

            fire = new Fire(fireTexture, 1300, -20);
            fire1 = new Fire(fireTexture, 1800, -20);

            letreroTitan = new LetreroTitan((Texture)manager.get("titanBoton.png"), 550,(4*ALTO)/5 -50);
            timeOver = 120f;
            enemyWait = 5;

            bombWait = 28f;
            gorathLimit = 5;
            bulletTexture = manager.get("runner/bullet.png");
            texturaNave = manager.get("Collect/naves.png");
            atoTime = 25f;
            musicaFondo = manager.get("sounds/Drive.mp3");

            bateriaInfo = new BitmapFont(Gdx.files.internal("ethno.fnt"));
            bateriaInfo.setColor(Color.BLUE);
        }
        else if(level == Nivel.FINALBOSS)
        {
            boltTexture = manager.get("Doom/plasmaball.png");
            bulletTexture = manager.get("Doom/proyectile.png");

            hit = manager.get("sounds/golpe.mp3");
            texturaDoom = manager.get("Doom/dooms.png");
            doom = new Doom(texturaDoom,Pantalla.ANCHO/4, (Pantalla.ALTO/3)*2);
            System.out.println("Cargo doom");
            escudosDoom = new ArrayList<Barrera>();
            texturaEscudoDoom = manager.get("Doom/sheet.png");

            escudoDoom = new Barrera(texturaEscudoDoom, doom);
            escudosDoom.add(escudoDoom);

            enemyWait = 0.2f;
            enemyTime = 0;
            fireTexture = manager.get("Fondos/FireSprite.png");
            texturaFondo1 = manager.get("Fondos/fondoN3.jpg");
            texturaFondo2 = manager.get("Fondos/fondoN3ed.png");

            fondo1 = new Fondo(texturaFondo1,275);
            fondo2 = new Fondo(texturaFondo2, 350);

            fire = new Fire(fireTexture, 1300, -20);
            fire1 = new Fire(fireTexture, 1800, -20);

            letreroTitan = new LetreroTitan((Texture)manager.get("titanBoton.png"), 550,(4*ALTO)/5 -50);
            timeOver = 180f;

            bombWait = 35f;
            bombTime = 5;
            gorathLimit = 5;

            atoTime = 12f;
            musicaFondo = manager.get("sounds/Fall.mp3");
            tiempoFire = 4f;

        }

        texturaFondo = manager.get("Fondos/piso.png");
        baterias = new Array<Bateria>();

        texturaGorath = manager.get("goraths.png");
        texturaBala = manager.get("runner/plasma.png");

        texturaRoja = manager.get("runner/plasmaRoja.png");

        texturaEscudo = manager.get("runner/escudo.png");
        texturabomba = new Texture("bomba.png");
        texturaGranada = manager.get("runner/plasma.png");

        fondo = new Fondo(texturaFondo, 370);

        if(juego.redSkin < 5)
        {
            robotTexture= manager.get("robotWalk.png");
            brinco1 = manager.get("high1.png");
            brinco2 = manager.get("high2.png");

            skinMode = 0;
        }
        else
        {
            robotTexture= manager.get("redWalk.png");
            brinco1 = manager.get("red1.png");
            brinco2 = manager.get("red2.png");

            skinMode = 1;
        }
        robot = new RobotRunner(robotTexture, 120f, 63f, skinMode, brinco1, brinco2);

        pauseButton = new Texture("botones/pause.png");
        pauseObject = new Objeto(pauseButton, (4*ANCHO)/5 -65, (4*ALTO)/5 +35);

        shieldTexture = manager.get("botones/shieldBoton.png");
        shieldPress = manager.get("botones/shieldBoton1.png");
        shieldBtn = new Objeto(shieldTexture, (4*ANCHO)/5 +100, (4*ALTO)/5 + 35);
        shieldPressBtn = new Objeto(shieldPress, (4*ANCHO)/5 +100, (4*ALTO)/5 +35);

        Gdx.input.setInputProcessor(new ProcesadorEntrada());

        startTime = TimeUtils.nanoTime();

        naveWait = 9f;
        enemyQuantity =1;
        naveQuantity = 3;
        enemyTime = 0;

        atoCharges = new Array<AtoCharges>();
        atoTexture = manager.get("Collect/atos.png");


        orbs = new Array<Orb>();
        orbTexture = manager.get("Collect/orbs.png");
        orbTime = 13f;


        TimePlaying = 0;
        bombTime = 0;

        energiaInfo = new BitmapFont(Gdx.files.internal("ethno.fnt"));
        energiaInfo.setColor(Color.BLUE);

        score = 0;
        scoreInfo = new BitmapFont(Gdx.files.internal("ethno.fnt"));

        scoreInfo.setColor(Color.BLUE);

        gorathNumber = 0;
        gorathNO = new BitmapFont(Gdx.files.internal("ethno.fnt"));
        gorathNO.setColor(Color.BLUE);


        barraVida1 = new Objeto((Texture)manager.get("barraVida1.png"),310,660);


        barraBlack = new Objeto((Texture)manager.get("barraBlack.png"),309,658);

        musicaFondo.setLooping(true);
        musicaFondo.play();
        titanMode = false;

    }



    @Override
    public void render(float delta) {
        Color cc;
        if(robot.getEnergia()>60)
        {
            cc = Color.GREEN;
        }
        else if(robot.getEnergia()>30 && robot.getEnergia()<60)
        {
            cc = Color.YELLOW;
        }
        else
        {
            cc = Color.RED;
        }

        camara.update();
        shapeRenderer.setProjectionMatrix(camara.combined);
        float ww = 230 * (robot.getEnergia()/100);


        // Actualizar
        if(estado==EstadoJuego.JUGANDO)
        {
            if(level == Nivel.FINALBOSS) {
                actualizarDoom(delta);
                actualizarBullets(delta);
                actualizarBulletsDiag(delta);
                actualizarBolts(delta);

            }

            updateJump(delta);
            actualizarEnemigos(delta);
            actualizarBalas(delta);
            actualizarBomba(delta);
            actualizarEscudo();
            actualizarItems(delta);
            actualizarBullets(delta);
            actualizarGranadas(delta);
            TimePlaying += delta;

            if(gorathNumber>40)
            {
                gorathNumber = 40;
            }

            if(level == Nivel.FINALBOSS && doom.getVida() <= 0)
            {
                musicaFondo.stop();
                juego.lastPlayed = 3;
                juego.scoreFinal = score;
                juego.setScreen(new Historia(juego, Pantallas.WINNER3));
            }

            if(TimePlaying>timeOver) {
                musicaFondo.pause();
                if (level == Nivel.LEVEL1 && gorathNumber > 39) {
                    musicaFondo.stop();

                    if (score > 4500) {
                        juego.shieldLoss = 0.02f;
                        juego.shootLoss = 0.5f;
                    }
                    juego.scoreFinal = score;
                    juego.lastPlayed = 1;
                    juego.setScreen(new Ganador(juego));
                }
                else if (level == Nivel.LEVEL1 && gorathNumber <40)
                {
                    juego.setScreen(new GameOver(juego));
                }
                else if (level == Nivel.LEVEL2 && bateriaCount <10)
                {
                    juego.setScreen(new GameOver(juego));
                }


            }
               else if(level == Nivel.LEVEL2 && bateriaCount>=10)
                {
                    musicaFondo.stop();
                    juego.scoreFinal = score;
                    if(score >= 9500)
                    {
                        juego.danio = 3.5f;
                    }
                    juego.lastPlayed = 2;
                    juego.setScreen(new Historia(juego, Pantallas.WINNER2));

                }
            else if(level == Nivel.FINALBOSS && doom.getVida()==0){
                musicaFondo.stop();
                juego.scoreFinal = score;
                juego.setScreen(new Historia(juego, Pantallas.WINNER3));
            }

            if(estadoPotencia == Power.ON)
            {
                potenciaTime += delta;
            }
            for( BulletDiag bulletDiag: bulletsDiag)
            {
                bulletDiag.mover(delta);
            }

        }

        // Dibujar
        borrarPantalla();
        batch.setProjectionMatrix(camara.combined);

        batch.begin();


        fondo1.dibujar(batch, delta, estado);

        if(level == Nivel.LEVEL2 || level == Nivel.FINALBOSS)
        {
            fire.dibujar(batch, estado);
            fire1.dibujar(batch, estado);
            if(estado == EstadoJuego.JUGANDO)
            {
                fire.mover(delta);
                fire1.mover(delta);
            }
            if(TimePlaying>tiempoFire)
            {
                fire.setX(1500);
                fire.setX(1800);
                tiempoFire += 10f;
            }


    }

        fondo2.dibujar(batch, delta, estado);

        fondo.dibujar(batch, delta,estado);
        robot.dibujar(batch, estado);



        if(level == Nivel.FINALBOSS){
            doom.dibujar(batch, estado);
            if(doom.getEstadoVida() == Doom.VIDA.MITAD){
                escudoDoom.dibujar(batch);
            }else if(doom.getEstadoVida() == Doom.VIDA.BAJA){
                escudosDoom.clear();
            }

        }

        if(estadoPotencia == Power.ON)
        {
            letreroTitan.dibujar(batch);
        }

        for(Granada granada: granadas){
            granada.dibujar(batch, estado, titanMode);
        }

        barraBlack.dibujar(batch);


        // Dibujar enemigo
        for (Gorath gorath : enemies) {
            gorath.dibujar(batch, estado);
        }
        for (Bala bala : balas) {
            bala.dibujar(batch, estado, titanMode);
        }
        for( BulletDiag bulletDiag: bulletsDiag)
        {
            bulletDiag.dibujar(batch);
        }
        for(Bolt bolt: bolts)
        {
            bolt.dibujar(batch, estado);
        }

        for (Bullet bullet : bullets) {
            bullet.dibujar(batch);
        }
        for(Orb orb: orbs)
        {
            orb.dibujar(batch, estado);
        }

        for(Bateria bateria: baterias)
        {
            bateria.dibujar(batch);
        }
        for(Bomba bomba : bombas)
        {
            bomba.dibujar(batch, estado);
        }
        for(Nave nave: naves)
        {
            nave.dibujar(batch, estado);
        }
        pauseObject.dibujar(batch);

        for(AtoCharges ato: atoCharges)
        {
            ato.dibujar(batch, estado);
        }


        if(robot.estadoProteccion == Personaje.EstadoProteccion.PROTEGIDO)
        {
            escudos.get(0).dibujar(batch);
            shieldPressBtn.dibujar(batch);
        }
        else
        {
            shieldBtn.dibujar(batch);
        }


        String energy = "Energia: ";
        energiaInfo.draw(batch, energy, 40, 695);

        String marcador = "SCORE: "+score;
        scoreInfo.draw(batch, marcador, 560,695);

        String gorathStr = gorathNumber + "/40";
        if(level == Nivel.LEVEL1)
        {
            gorathNO.draw(batch, gorathStr, 50,630);
            gorathSymbol.dibujar(batch);
        }
        else if(level == Nivel.LEVEL2)
        {
            String bateriaNum = bateriaCount + "/10";
            bateriaInfo.draw(batch, bateriaNum, 50, 630);
            bateriaLogo.dibujar(batch);
        }

        if ( estado == EstadoJuego.PAUSADO)
        {
            pausaLogo.mostrarMensaje(batch, "PAUSA", 590, 550);
        }

        barraVida1.dibujar(batch);

        robot.danio();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(cc);
        shapeRenderer.rect(310,660,ww,44);

        shapeRenderer.end();




        batch.end();



        if(robot.getEnergia() <=0)
        {
            musicaFondo.stop();
            juego.setScreen(new GameOver(juego));

        }

        if (escenaPausa!=null && estado == EstadoJuego.PAUSADO)
        {
            escenaPausa.draw();

        }
        else
        {
            escenaPausa = null;
        }

        if(estadoPotencia== Power.ON)
        {
            titanMode = true;
            potencia = 5;
        }
        else
        {
            potencia = 3;
        }


        if((potenciaTime)>20f)
        {
            estadoPotencia = Power.OFF;
            titanMode = false;
        }

    }


    private void actualizarDoom(float delta){
        doom.vivir(delta);
        doom.mover(delta);
        if(MathUtils.random(0, 55) == 10) {
            if(doom.getEstadoVida() == Doom.VIDA.COMPLETO
                    || doom.getEstadoVida() == Doom.VIDA.MITAD){
                if(doom.getEstadoVida() == Doom.VIDA.COMPLETO)
                {
                    shoot((int) doom.sprite.getX(), (int) doom.sprite.getY());
                }
                else
                {
                    if(MathUtils.random(0,7)>4)
                    {
                        shootBolt((int) doom.sprite.getX(), (int) doom.sprite.getY());
                    }

                }

            }else if(doom.getEstadoVida() == Doom.VIDA.BAJA)
            {
                hordeTime += delta;

                if(hordeTime>10)
                {
                    gorathHorde = false;
                }
                else
                {
                    gorathHorde = true;
                }
                if(MathUtils.random(0,5)>2)
                {
                    shootDiag((int) doom.sprite.getX(), (int) doom.sprite.getY());
                }

            }
        }
    }



    private void actualizarGranadas(float delta){
        for(int i=granadas.size-1; i>=0; i--) {
            Granada granada = granadas.get(i);
            granada.mover(delta);
            if (granada.sprite.getX()>ANCHO+50) {
                // Salede la pantalla
                granadas.removeIndex(i);
                break;
            }

            // Prueba choque contra todos los enemigos
            for (int j=enemies.size-1; j>=0; j--) {
                Gorath gorath = enemies.get(j);

                if (granada.chocaCon(gorath) && gorath.life == Gorath.Life.ALIVE) {
                    // La bala desaparece
                    gorath.kill();
                    if(estadoSonido == EstadoSonido.SOUND)
                    {
                        gorathDeath.play(0.6f);
                    }

                    gorathNumber++;
                    score += 100;
                    granadas.removeIndex(i);
                    if(estadoSonido == EstadoSonido.SOUND)
                    {
                        granadeSound.play();
                    }

                    break;  // Siguiente bala, ésta ya no existe
                }
                if(gorath.muerto)
                {
                    enemies.removeIndex(j);
                }
            }

        }
        for(int i=granadas.size-1; i>=0; i--) {
            Granada granada = granadas.get(i);
            for (int j=naves.size-1; j>=0; j--) {
                Nave nave = naves.get(j);

                if (granada.chocaCon(nave) &&  nave.life == Nave.Life.ALIVE) {
                    // La bala desaparece
                    nave.setVida(-potencia);

                    granadas.removeIndex(i);
                    if(estadoSonido == EstadoSonido.SOUND)
                    {
                        granadeSound.play();
                    }

                    break;  // Siguiente bala, ésta ya no existe
                }
            }
        }
        if(level == Nivel.FINALBOSS)
        {


            for(int i=granadas.size-1; i>=0; i--){
                Granada granada = granadas.get(i);
                if (granada.chocaCon(doom)){
                    if(estadoSonido == EstadoSonido.SOUND)
                    {
                        hit.play(0.8f);
                    }
                    doom.setVida(doom.getVida()-potencia*0.5);
                    System.out.println(doom.getVida());
                    granadas.removeIndex(i);
                    break;  // Siguiente bala, ésta ya no existe
                }
            }
            for(int i=granadas.size-1; i>=0; i--){
                Granada granada = granadas.get(i);
                if(escudoDoom.chocaCon(granada)){
                    granadas.removeIndex(i);
                    break;
                }
            }
        }


    }


    private void actualizarBullets(float delta)
    {
        for(int i=bullets.size-1; i>=0; i--) {
            Bullet bullet = bullets.get(i);
            bullet.mover(delta);
            if (bullet.sprite.getX() < -50) {
                // Salede la pantalla
                bullets.removeIndex(i);
                break;
            }
            if (bullet.chocaCon(robot) && bullet.sprite.getX()-robot.sprite.getX()<20) {

                robot.setEnergia(robot.getEnergia()-(float)(damage*0.4));
                robot.daniar();
                bullets.removeIndex(i);
                break;  // Siguiente bala, ésta ya no existe
            }
            if (escudos.size>0 && bullet.chocaCon(escudos.get(0)))
            {

                bullets.removeIndex(i);
                break;  // Siguiente bala, ésta ya no existe
            }
        }
    }


    private void actualizarBolts(float delta)
    {
        for(int i=bolts.size-1; i>=0; i--) {
            Bolt bolt = bolts.get(i);
            bolt.mover(delta);
            if (bolt.sprite.getX() < -50) {
                // Salede la pantalla
                bolts.removeIndex(i);
                break;
            }
            if (bolt.chocaCon(robot) && bolt.sprite.getX()-robot.sprite.getX()<20) {

                robot.setEnergia(robot.getEnergia()-(float)(damage*2));
                robot.daniar();
                bolts.removeIndex(i);
                break;  // Siguiente bala, ésta ya no existe
            }
            if (escudos.size>0 && bolt.chocaCon(escudos.get(0)))
            {
                robot.setEnergia(robot.getEnergia()-(float)(damage));
                robot.daniar();
                bolts.removeIndex(i);
                break;  // Siguiente bala, ésta ya no existe
            }
        }
    }


    private void actualizarBulletsDiag(float delta){
        for(int i=bulletsDiag.size-1; i>=0; i--){
            BulletDiag bullet = bulletsDiag.get(i);
            bullet.mover(delta);
            if (bullet.sprite.getX() < -50){
                // Sale de la pantalla
                bulletsDiag.removeIndex(i);
                break;
            }
            if (bullet.chocaCon(robot) && bullet.sprite.getX()-robot.sprite.getX()<20){
                robot.setEnergia(robot.getEnergia()-(float)(damage*0.4));
                robot.daniar();
                bulletsDiag.removeIndex(i);
                break;  // Siguiente bala, ésta ya no existe
            }
            if (escudos.size>0 && bullet.chocaCon(escudos.get(0))){
                bulletsDiag.removeIndex(i);
                break;  // Siguiente bala, ésta ya no existe
            }
        }
    }


    private void actualizarEscudo(){
        if(robot.getEstadoProteccion()== Personaje.EstadoProteccion.PROTEGIDO){
            robot.setEnergia((float)(robot.getEnergia()-juego.shieldLoss));


        }
        if(escudos.size>0) {
            escudos.get(0).actualizarMovimiento();
            for (int j = enemies.size - 1; j >= 0; j--) {
                Gorath gorath = enemies.get(j);
                if (escudos.get(0).chocaCon(gorath) && gorath.life == Gorath.Life.ALIVE) {
                    escudos.clear();

                    robot.setEstadoProteccion(Personaje.EstadoProteccion.NO_PROTEGIDO);
                    gorath.kill();
                    if(estadoSonido == EstadoSonido.SOUND)
                    {
                        gorathDeath.play(0.6f);
                    }
                    break;
                }
                if(gorath.muerto)
                {
                    enemies.removeIndex(j);
                }
            }
        }
        if(escudos.size>0)
        {
            for (int j=naves.size-1; j>=0; j--){
                Nave nave = naves.get(j);
                if (escudos.get(0).chocaCon(nave)&& nave.life == Nave.Life.ALIVE) {
                    escudos.clear();

                    robot.setEstadoProteccion(Personaje.EstadoProteccion.NO_PROTEGIDO);
                    nave.setVida(-6);
                    break;
                }
            }
        }
    }


    private void actualizarBalas(float delta) {
        for(int i=balas.size-1; i>=0; i--) {
            Bala bala = balas.get(i);
            bala.mover(delta);
            if (bala.sprite.getX() > ANCHO + 100) {
                // Salede la pantalla
                balas.removeIndex(i);
                break;
            }

            // Prueba choque contra todos los enemigos
            for (int j = enemies.size - 1; j >= 0; j--) {
                Gorath gorath = enemies.get(j);

                if (bala.chocaCon(gorath) && gorath.life == Gorath.Life.ALIVE) {
                    // La bala desaparece
                    gorath.kill();
                    if(estadoSonido == EstadoSonido.SOUND)
                    {
                        gorathDeath.play(0.6f);
                    }
                    if(level == Nivel.LEVEL1 && gorathNumber<40)
                    {
                        gorathNumber++;
                    }
                    score += 100;
                    balas.removeIndex(i);
                    break;  // Siguiente bala, ésta ya no existe
                }
                if(gorath.muerto)
                {
                    enemies.removeIndex(j);
                }
            }
        }
        for(int i=balas.size-1; i>=0; i--) {
            Bala bala = balas.get(i);
            for (int j=naves.size-1; j>=0; j--) {
                Nave nave = naves.get(j);

                if (bala.chocaCon(nave) && nave.life == Nave.Life.ALIVE) {
                    // La bala desaparece
                    nave.setVida(-potencia);
                    balas.removeIndex(i);
                    break;  // Siguiente bala, ésta ya no existe
                }
            }
        }


        if(level == Nivel.FINALBOSS)
        {
            for(int i=balas.size-1; i>=0; i--){
                    Bala bala = balas.get(i);
                if (bala.chocaCon(doom)){
                    if(estadoSonido == EstadoSonido.SOUND)
                    {
                        hit.play(0.8f);
                    }

                    doom.setVida(doom.getVida()-potencia*0.5);
                    System.out.println(doom.getVida());
                    balas.removeIndex(i);
                    break;  // Siguiente bala, ésta ya no existe
                }
            }

            for(int i=balas.size-1; i>=0; i--){
                Bala bala = balas.get(i);
                if(escudoDoom.chocaCon(bala)){
                    balas.removeIndex(i);
                    break;
                }
            }

        }

    }


    private void actualizarBomba(float delta)
    {
        if(bombTime<TimePlaying)
        {
            bombTime += bombWait;
            Bomba bomba = new Bomba(texturabomba, ANCHO+20, 65);
            bombas.add(bomba);


        }
        for(Bomba b: bombas)
        {
            if(b.chocaCon(robot))
            {
                bombaTolerance += delta;
                System.out.println("tolerance: "+bombaTolerance);
                if(bombaTolerance >= 0.2)
                {
                    bombaTolerance = 0;
                    robot.setEnergia(0);
                    bombas.clear();
                }

            }
            if(b.getX()<10)
            {
                bombas.clear();
                bombaTolerance = 0;
            }
            b.mover(delta);
        }

    }


    private void actualizarItems(float delta)
    {
        if(TimePlaying>atoTime)
        {

            AtoCharges ato = new AtoCharges(atoTexture, ANCHO+20, 90*MathUtils.random(3,6));
            atoCharges.add(ato);
            if(level == Nivel.LEVEL1)
            {
                atoTime += 20;
            }
            else if(level == Nivel.LEVEL2)
            {
                atoTime += 15f;
            }
            else if(level == Nivel.FINALBOSS)
            {
                atoTime += 8f;
            }

        }
        for(AtoCharges a: atoCharges)
        {
            if(a.chocaCon(robot))
            {
                robot.setEnergia(robot.getEnergia()+20);

                if(estadoSonido == EstadoSonido.SOUND)
                {
                    collect.play(0.6f);
                }

                atoCharges.clear();
            }
            if(a.getX()<10)
            {
                atoCharges.clear();
            }
            a.mover(delta);
        }

        if(level != Nivel.LEVEL1)
        {
            if(TimePlaying>orbTime)
            {
                Orb orb = new Orb(orbTexture, ANCHO+20,100*MathUtils.random(3,5));
                orbs.add(orb);
                orbTime += 11;
            }

            for(Orb o: orbs)
            {
                if(o.chocaCon(robot))
                {
                    if(estadoSonido == EstadoSonido.SOUND)
                    {
                        collect.play(.6f);
                    }

                    if(estadoPotencia == Power.OFF)
                    {
                        estadoPotencia = Power.ON;
                        potenciaTime = 0;
                    }

                    orbs.clear();
                }
                if(o.getX()<10)
                {
                    orbs.clear();
                }
                o.mover(delta);
            }
        }

        if(level == Nivel.LEVEL2)
        {
            if(TimePlaying>bateriaTime)
            {
                Bateria bateria = new Bateria(bateriaTexture, ANCHO+20,110*MathUtils.random(2,5));
                baterias.add(bateria);
                bateriaTime += 9;
            }

            for(Bateria bateria: baterias)
            {
                if(bateria.chocaCon(robot))
                {
                    if(estadoSonido == EstadoSonido.SOUND)
                    {
                        collect.play(.6f);
                    }

                    if(level == Nivel.LEVEL2)
                    {
                        bateriaCount += 1;
                    }

                    baterias.clear();
                }
                if(bateria.getX()<0)
                {
                    baterias.clear();
                }
                bateria.mover(delta);
            }
        }

    }


    private void actualizarEnemigos(float delta)
    {
        // Generar nuevo enemigo
        enemyTime += delta;
        if(gorathHorde)
        {
            if(enemyTime>0.5)
            {
                if(altoGorath == 60)
                {
                    altoGorath = 400;
                }
                else if(altoGorath == 400)
                {
                    altoGorath = 60;
                }

                Gorath gorath = new Gorath(texturaGorath, texturaExplosion, ANCHO +100, altoGorath,3, -700);
                enemies.add(gorath);
                enemyTime = 0;
            }

        }
        else if(level != Nivel.FINALBOSS)
        {
            if(enemyTime>enemyWait && TimePlaying<53f ) {
                enemyTime = 0;
                for (int i = 0; i < enemyQuantity; i++) {
                    int distanciaExtra = MathUtils.random(190, 250);
                    Gorath gorath = new Gorath(texturaGorath, texturaExplosion, ANCHO + (i * distanciaExtra),
                            65 * MathUtils.random(1, 5), MathUtils.random(1,4), -600);
                    enemies.add(gorath);
                }
                if (enemyQuantity < gorathLimit) {
                    enemyQuantity += 1;
                }

                if (enemyQuantity == 4) {
                    enemyWait -= 1;
                }
            }
        }


        // Actualizar enemigos
        for (Gorath gorath :enemies) {
            gorath.mover(delta);

        }
        // Verificar choque
        for(int k=enemies.size-1; k>=0; k--) {
            Gorath gorath = enemies.get(k);
            if(gorath.sprite.getX()<5)
            {
                enemies.removeIndex(k);
            }
            if (gorath.sprite.getX()<-gorath.sprite.getWidth() || gorath.muerto) {
                enemies.removeIndex(k);
            }
            else if (gorath.clash(robot) && gorath.sprite.getX()-robot.sprite.getX()<30 && gorath.life == Gorath.Life.ALIVE) {
                // Pierde!!!
                gorath.kill();
                if(estadoSonido == EstadoSonido.SOUND)
                {
                    gorathDeath.play(0.6f);
                }
                System.out.println("energia: "+robot.getEnergia());
                // Activar escenaPausa y pasarle el control
                robot.setEnergia((float)(robot.getEnergia()-damage));
                robot.daniar();
            }
        }


        if(level == Nivel.LEVEL2)
        {
            if(TimePlaying>naveWait && TimePlaying<108 )
            {
                naveWait += 8;
                for(int i =0; i<(naveQuantity/3); i++)
                {
                    int distanciaExtra = MathUtils.random(330,350);
                    Nave nave = new Nave(texturaNave, texturaExplosion, ANCHO+(i*distanciaExtra), 85*MathUtils.random(1,4));
                    naves.add(nave);
                }
                if(naveQuantity<naveLimit)
                {
                    naveQuantity += 1;
                }

                if(TimePlaying>83f)
                {
                    naveWait -= 1f;
                }
                if(TimePlaying>100)
                {
                    naveQuantity = 3;
                }
            }


            // Actualizar enemigos
            for (Nave nave :naves)
            {
                nave.mover(delta);
            }
            // Verificar choque
            for(int k=naves.size-1; k>=0; k--) {
                Nave nave = naves.get(k);
                if(nave.muerto)
                {
                    score += 250;
                    naves.removeIndex(k);
                }
                else
                {
                    if(nave.getVida() <=0)
                    {
                        nave.kill();

                        continue;
                    }
                    if(nave.sprite.getX()<0)
                    {
                        naves.removeIndex(k);
                    }

                    if(nave.getEstadoShoot()==Nave.EstadoShoot.AGGRESSIVE)
                    {
                        nave.setEstadoShoot(Nave.EstadoShoot.PASSIVE);
                        shoot((int)nave.sprite.getX(),(int)nave.sprite.getY());
                    }
                    if (nave.clash(robot) && nave.sprite.getX()-robot.sprite.getX()<30) {
                        // Pierde!!!
                        nave.setVida(-5);
                        robot.daniar();
                        // Activar escenaPausa y pasarle el control
                        robot.setEnergia((float)(robot.getEnergia()-(damage *1.2)));

                    } else if (nave.sprite.getX()<-nave.sprite.getWidth()) {
                        naves.removeIndex(k);
                    }
                }

            }
        }

    }



    private void updateJump(float delta) {
        robot.actualizar(delta);
    }


    private void disparar() {
        if (TimeUtils.timeSinceNanos(startTime) > 210000000) {

            if(estadoSonido == EstadoSonido.SOUND)
            {
                gun.play(0.4f);
            }

            robot.setEnergia(robot.getEnergia()-juego.shootLoss);
            //System.out.println("loss "+juego.shootLoss);
            Bala bala = new Bala(texturaBala, texturaRoja,
                    robot.sprite.getX() + robot.sprite.getWidth() / 2,
                    robot.sprite.getY() + robot.sprite.getHeight() / 2 - texturaBala.getHeight() / 2);
            balas.add(bala);
            startTime = TimeUtils.nanoTime();
        }
    }


    private void shoot(int x, int y)
    {
        Bullet bullet = new Bullet(bulletTexture, x, y);
        bullets.add(bullet);

        if(estadoSonido == EstadoSonido.SOUND)
        {
            lazer.play(0.4f);
        }

    }


    private void shootBolt(int x, int y)
    {
        Bolt bolt = new Bolt(boltTexture, x, y);
        bolts.add(bolt);

    }


    private void shootDiag(int x, int y)
    {
        BulletDiag bullet = new BulletDiag(bulletTexture, x, y);
        bulletsDiag.add(bullet);
        if(estadoSonido == EstadoSonido.SOUND) {lazer.play(0.4f);}
    }


    private void bombear(float y) {
        if (TimeUtils.timeSinceNanos(startTime) > 200000000) {
            if(estadoSonido == EstadoSonido.SOUND)
            {
                gun.play(0.7f);
            }

            robot.setEnergia(robot.getEnergia()-juego.shootLoss);
            Granada granada = new Granada(texturaGranada, texturaRoja,
                    robot.sprite.getX() + robot.sprite.getWidth() / 2,
                    robot.sprite.getY() + robot.sprite.getHeight() / 2 - texturaGranada.getHeight() / 2, y);
            granadas.add(granada);
            startTime = TimeUtils.nanoTime();
        }
    }


    private void proteger(){
        if(escudos.size==0) {
            if(estadoSonido == EstadoSonido.SOUND)
            {
                shieldSound.play();
            }

            Escudo escudo = new Escudo(texturaEscudo,
                    (robot.sprite.getX() - robot.sprite.getWidth() / 2)+20,
                    robot.sprite.getY() -7, robot);
            escudos.add(escudo);
            robot.setEstadoProteccion(Personaje.EstadoProteccion.PROTEGIDO);
        }else if(escudos.size>0){
            escudos.clear();
            robot.setEstadoProteccion(Personaje.EstadoProteccion.NO_PROTEGIDO);
        }
    }


    @Override
    public void pause() {

    }


    @Override
    public void resume() {

    }


    @Override
    public void dispose() {

    }


    private class ProcesadorEntrada implements InputProcessor
    {
        private Vector3 v = new Vector3();


        @Override
        public boolean touchDown(int screenX, int screenY, int pointer, int button) {
            v.set(screenX, screenY, 0);
            camara.unproject(v);

            boolean puedeDisparar = robot.getEstadoSalto() == Personaje.EstadoSalto.EN_PISO;
            boolean puedeLanzar = robot.getEstadoSalto() == Personaje.EstadoSalto.SALTANDO;

            if(pauseObject.contiene(v))
            {
                estado = EstadoJuego.PAUSADO;
                escenaPausa = new EscenaPausa(vista,batch);

                Gdx.input.setInputProcessor(escenaPausa);
            }
            else if(shieldBtn.contiene(v))
            {
                proteger();
            }
            else if (v.x<ANCHO/2 && v.y<(5*ALTO)/6) {
                robot.saltar();
            }
            else if(v.x>ANCHO/2 && v.y<(4*ALTO)/5){
                if(puedeDisparar){
                    disparar();
                }else if(puedeLanzar){
                    bombear(v.y);
                }
            }

            return true;
        }

        @Override
        public boolean touchUp(int screenX, int screenY, int pointer, int button) {
            if (pointer == punteroHorizontal) {
                punteroHorizontal = INACTIVO;
               // dx = 0; // Deja de moverse en x
            } else if (pointer == punteroVertical) {
                punteroVertical = INACTIVO;
                //dy = 0; // Deja de moverse en y
            }
            return true;
        }

        @Override
        public boolean touchDragged(int screenX, int screenY, int pointer) {
            return true;
        }

        @Override
        public boolean mouseMoved(int screenX, int screenY) {
            return false;
        }

        @Override
        public boolean scrolled(int amount) {
            return false;
        }

        @Override
        public boolean keyDown(int keycode) {
            return false;
        }

        @Override
        public boolean keyUp(int keycode) {
            return false;
        }

        @Override
        public boolean keyTyped(char character) {
            return false;
        }
    }


    // La escena que se muestra cuando el juego se pausa
    // (simplificado, ver la misma escena en PantallaWhackAMole)
    private class EscenaPausa extends Stage
    {
        public EscenaPausa(Viewport vista, SpriteBatch batch) {
            super(vista, batch);
            // Crear rectángulo transparente
            Pixmap pixmap = new Pixmap((int)(ANCHO*0.7f), (int)(ALTO*0.8f), Pixmap.Format.RGBA8888 );
            pixmap.setColor( 0.1f, 0.1f, 0.1f, 0.65f );

            pixmap.fillRectangle(0, 0, pixmap.getWidth(), pixmap.getHeight());
            Texture texturaRectangulo = new Texture( pixmap );
            pixmap.dispose();
            final Image imgRectangulo = new Image(texturaRectangulo);
            imgRectangulo.setPosition(0.15f*ANCHO, 0.1f*ALTO);
            this.addActor(imgRectangulo);

            Texture reanudar = new Texture(Gdx.files.internal("botones/reanudar.png"));
            Texture reanudarPress =  new Texture(Gdx.files.internal("botones/reanudar1.png"));

            ImageButton reaudarBtn = new ImageButton(new TextureRegionDrawable(new TextureRegion(reanudar)),
                    new TextureRegionDrawable((new TextureRegion(reanudarPress))));
            reaudarBtn.addListener(new ActorGestureListener() {

                public void tap(InputEvent event, float x, float y, int count, int button)
                {
                    if(estadoSonido == EstadoSonido.SOUND)
                    {
                        menu.play();
                    }

                    super.tap( event,  x,  y,  count, button);
                    estado = EstadoJuego.JUGANDO;

                    Gdx.input.setInputProcessor(new ProcesadorEntrada());

                }
            });

            Texture deshabilitar = new Texture(Gdx.files.internal("botones/deshabilitar.png"));
            Texture deshabilitarPress =  new Texture(Gdx.files.internal("botones/deshabilitar1.png"));

            ImageButton deshabBtn = new ImageButton(new TextureRegionDrawable(new TextureRegion(deshabilitar)),
                    new TextureRegionDrawable((new TextureRegion(deshabilitarPress))));
            deshabBtn.addListener(new ActorGestureListener() {

                public void tap(InputEvent event, float x, float y, int count, int button)
                {
                    menu.play();
                    switch (estadoSonido)
                    {
                        case SILENCE:
                            musicaFondo.play();
                            estadoSonido = EstadoSonido.SOUND;
                            break;
                        case SOUND:
                            musicaFondo.pause();
                            estadoSonido = EstadoSonido.SILENCE;
                    }

                }
            });


            Texture salir = new Texture(Gdx.files.internal("botones/salir.png"));
            Texture salirPress =  new Texture(Gdx.files.internal("botones/salir1.png"));

            ImageButton salBtn = new ImageButton(new TextureRegionDrawable(new TextureRegion(salir)),
                    new TextureRegionDrawable((new TextureRegion(salirPress))));
            salBtn.addListener(new ActorGestureListener() {

                public void tap(InputEvent event, float x, float y, int count, int button)
                {
                    if(estadoSonido == EstadoSonido.SOUND)
                    {
                        menu.play();
                    }

                    super.tap( event,  x,  y,  count, button);
                    juego.setScreen(new MenuPrincipal(juego));
                    musicaFondo.stop();

                }
            });

            Table table = new Table();
            table.row();//siguiente fila
            table.add(reaudarBtn).padBottom(25f).colspan(1).expandX().uniform();
            table.row();
            table.add(deshabBtn).padBottom(25f).colspan(1).expandX().uniform();
            table.row();
            table.add(salBtn).padBottom(25f).colspan(1).expandX().uniform();


            table.setFillParent(true);
            table.pack();

            this.addActor(table);
        }
    }

    private enum Nivel
    {
        LEVEL1,
        LEVEL2,
        FINALBOSS
    }

    private enum Power
    {
        ON,
        OFF
    }

    private enum EstadoSonido
    {
        SILENCE,
        SOUND
    }

}
