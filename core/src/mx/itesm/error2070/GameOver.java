package mx.itesm.error2070;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.FillViewport;

/**
 * Created by Daniel Alillo on 30/10/2017.
 */

public class GameOver extends ScreenAdapter
{
    private final Error game;
    private static final float WORDL_WIDTH = 1280;
    private static final float WORDL_HEIGHT = 720;

    private final AssetManager manager;
    private Table table;

    private Texture backgroundTexture;
    private Texture atras;//para regrsar al menuprincipal
    private Texture atrasPress;

    private Stage stage;
    private Sound laugh;


    public GameOver(Error game)
    {

        this.game = game;
        manager = game.getAssetManager();
    }


    @Override
    public void show() {
        super.show();
        stage = new Stage(new FillViewport(WORDL_WIDTH, WORDL_HEIGHT));
        Gdx.input.setInputProcessor(stage);

        backgroundTexture = new Texture(Gdx.files.internal("Fondos/gameover.jpg"));//nombres de los integrantes
        Image background = new Image((backgroundTexture));
        stage.addActor(background);

        atras = new Texture(Gdx.files.internal("botones/atras.png"));
        atrasPress =  new Texture(Gdx.files.internal("botones/atras1.png"));

        laugh = manager.get("sounds/risa.mp3");

        ImageButton atBtn = new ImageButton(new TextureRegionDrawable(new TextureRegion(atras)),
                new TextureRegionDrawable((new TextureRegion(atrasPress))));
        atBtn.addListener(new ActorGestureListener() {

            public void tap(InputEvent event, float x, float y, int count, int button)
            {
                super.tap( event,  x,  y,  count, button);
                game.setScreen(new SeleccionaNivel(game));
                dispose();
            }
        });
        laugh.play();

        table = new Table();

        table.add(atBtn).bottom().padBottom(25f).colspan(1).expandY();//se coloca al fondo

        table.setFillParent(true);
        table.pack();

        stage.addActor(table);
    }

    @Override
    public void render(float delta)
    {
        super.render(delta);
        clearScreen();
        stage.act(delta);
        stage.draw();

    }

    @Override
    public void resize(int width, int height)
    {
        super.resize(width, height);
        stage.getViewport().update(width, height);

    }


    private void clearScreen()
    {
        Gdx.gl.glClearColor((float)0, (float)0, 0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    public void dispose()
    {
        super.dispose();;
        stage.dispose();
        atras.dispose();
        backgroundTexture.dispose();
        atrasPress.dispose();

    }

}
