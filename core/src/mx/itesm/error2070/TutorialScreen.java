package mx.itesm.error2070;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.FillViewport;

/**
 * Created by Daniel Alillo on 12/10/2017.
 */

public class TutorialScreen extends ScreenAdapter
{
    private final Error game;
    private static final float WIDTH = 1280;
    private static final float HEIGHT = 720;
    private Stage stage;

    private Texture sig;
    private Texture sigPress;

    private Texture prev;
    private Texture prevPress;

    private Texture menu;
    private Texture menu1;

    private int fNumber = 0;
    private int fLimit;


    private final AssetManager manager;

    private ImageButton atBtn;
    private ImageButton atBtn1;

    private ImageButton me;

    private Image [] backgrounds;



    protected SpriteBatch batch;

    public TutorialScreen(Error game)
    {
        this.game = game;
        manager = game.getAssetManager();
    }

    @Override
    public void show() {
        super.show();
        stage = new Stage(new FillViewport(WIDTH, HEIGHT));

        Gdx.input.setInputProcessor(stage);
        backgrounds = new Image[15];

        menu =(manager.get("botones/botonMenu.png"));
        menu1 =  (manager.get("botones/botonMenu1.png"));

        sig =(manager.get("botones/flechaD.png"));
        sigPress =  (manager.get("botones/flechaD1.png"));

        prev =(manager.get("botones/flechaL.png"));
        prevPress =  (manager.get("botones/flechaL1.png"));

        backgrounds[0] = new Image((Texture)manager.get("Fondos/Instrucciones/Instructivo1.jpg"));
        backgrounds[1] = new Image((Texture)manager.get("Fondos/Instrucciones/Instructivo2.jpg"));
        backgrounds[2] = new Image((Texture)manager.get("Fondos/Instrucciones/Instructivo3.jpg"));
        backgrounds[3] = new Image((Texture)manager.get("Fondos/Instrucciones/Instructivo4.jpg"));
        backgrounds[4] = new Image((Texture)manager.get("Fondos/Instrucciones/Instructivo5.jpg"));

        stage.addActor(backgrounds[4]);
        stage.addActor(backgrounds[3]);
        stage.addActor(backgrounds[2]);
        stage.addActor(backgrounds[1]);
        stage.addActor(backgrounds[0]);

        fLimit = 4;


        atBtn = new ImageButton(new TextureRegionDrawable(new TextureRegion(sig)),
                new TextureRegionDrawable((new TextureRegion(sigPress))));
        atBtn.addListener(new ActorGestureListener() {

            public void tap(InputEvent event, float x, float y, int count, int button)
            {
                super.tap( event,  x,  y,  count, button);

                game.mennus.play();
                if(fNumber<fLimit)
                {
                    borrarScreen();
                    if(fNumber<fLimit -1)
                    {
                        fNumber++;
                    }

                }
                if(fNumber == fLimit -1)
                {
                    if(game.redSkin > 0)
                    {
                        game.redSkin -= 1;
                    }

                }


            }
        });
        atBtn.setPosition(950, 600);


        stage.addActor(atBtn);


        atBtn1 = new ImageButton(new TextureRegionDrawable(new TextureRegion(prev)),
                new TextureRegionDrawable((new TextureRegion(prevPress))));
        atBtn1.addListener(new ActorGestureListener() {

            public void tap(InputEvent event, float x, float y, int count, int button)
            {
                super.tap( event,  x,  y,  count, button);

                game.mennus.play();
                if(fNumber>0)
                {
                    borrarScreen();
                    fNumber--;

                }
                else if(game.redSkin <6)
                {
                    game.redSkin += 1;
                }


            }
        });
        atBtn1.setPosition(800, 600);

        stage.addActor(atBtn1);


        me = new ImageButton(new TextureRegionDrawable(new TextureRegion(menu)),
                new TextureRegionDrawable((new TextureRegion(menu1))));
        me.addListener(new ActorGestureListener() {

            public void tap(InputEvent event, float x, float y, int count, int button)
            {
                super.tap( event,  x,  y,  count, button);


                game.setScreen(new MenuPrincipal(game));
                dispose();

            }
        });
        me.setPosition(100, 30);

        stage.addActor(me);

    }

    @Override
    public void render(float delta)
    {
        super.render(delta);
        clearScreen();
        stage.act(delta);
        stage.draw();

    }

    @Override
    public void resize(int width, int height)
    {
        super.resize(width, height);
        stage.getViewport().update(width, height);

    }


    public void borrarScreen()
    {

        stage.clear();
        for(int i= fLimit;i>fNumber; i-- )
        {
            System.out.println("i: "+i);
            stage.addActor(backgrounds[i]);
        }
        stage.addActor(atBtn);
        stage.addActor(atBtn1);
        stage.addActor(me);

    }

    private void clearScreen()
    {
        Gdx.gl.glClearColor((float)0, (float)0, 0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    public void dispose()
    {
        super.dispose();;
        stage.dispose();


    }

}
