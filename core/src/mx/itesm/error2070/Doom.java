package mx.itesm.error2070;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.ArrayList;
import java.util.Random;


/**
 * Created by Aldo on 10/11/2017.
 */

public class Doom extends Objeto {

    private final float VELOCIDAD_EN_X = 600;
    private final float VELOCIDAD_EN_Y = 400;
    public ArrayList<Bullet> balasDoom;
    private int turnDanio;

    private Animation<TextureRegion> spriteAnimado;         // Animación volando
    private float timerAnimacion;

    public enum VIDA{
        COMPLETO,
        MITAD,
        BAJA,
        MUERTO
    }

    private boolean movDerPrim = true, movAbaPrim = false, movDiagPrim = false;
    private boolean movDerSeg = false, movAbaSeg = false, movDiagSeg = false;
    private double vida;
    private VIDA estadoVida;
    private float tiempoDaniado;
    private int permisoDanio;

    public Doom(Texture textura, float x, float y){
        // Lee la textura como región
        tiempoDaniado = 0;
        turnDanio = 0;
        TextureRegion texturaCompleta = new TextureRegion(textura);
        // La divide en 2 frames de 32x64 (ver enemigo.png)
        TextureRegion[][] texturaPersonaje = texturaCompleta.split(282,289);
        // Crea la animación con tiempo de 0.25 segundos entre frames.
        spriteAnimado = new Animation(0.5f, texturaPersonaje[0][0], texturaPersonaje[0][1],
                texturaPersonaje[0][2],texturaPersonaje[0][3],texturaPersonaje[1][0],
                texturaPersonaje[1][1],texturaPersonaje[1][2],texturaPersonaje[1][3]);
        // Animación infinita
        spriteAnimado.setPlayMode(Animation.PlayMode.LOOP);
        // Inicia el timer que contará tiempo para saber qué frame se dibuja
        timerAnimacion = 0;
        sprite = new Sprite(texturaPersonaje[0][0]);
        sprite.setPosition(x,y);


        vida = 115;
        estadoVida = VIDA.COMPLETO;
        balasDoom = new ArrayList<Bullet>();
        permisoDanio = 0;
    }


    public void dibujar(SpriteBatch batch, EstadoJuego estadoJuego)
    {
        // Dibuja la nave dependiendo del estadoMovimiento
        if(estadoJuego == EstadoJuego.JUGANDO)
        {
            if(estadoVida == VIDA.BAJA)
            {
                turnDanio +=1;
                if(turnDanio%12 == 0)
                {
                    permisoDanio += 1;
                }


                timerAnimacion += Gdx.graphics.getDeltaTime();
                // Frame que se dibujará
                tiempoDaniado += Gdx.graphics.getDeltaTime();
                TextureRegion region = spriteAnimado.getKeyFrame(timerAnimacion);

                if( permisoDanio%2==0)
                {
                    batch.setColor(Color.RED);
                }

                batch.draw(region,sprite.getX(),sprite.getY());

                batch.setColor(Color.WHITE);

            }
            else
            {
                timerAnimacion += Gdx.graphics.getDeltaTime();
                // Frame que se dibujará
                TextureRegion region = spriteAnimado.getKeyFrame(timerAnimacion);
                batch.draw(region, sprite.getX(), sprite.getY());
            }


        }

        else
        {
            sprite.draw(batch);
        }
    }

    public void vivir(float delta){
        if(this.vida <= 115 && this.vida >=85){
            estadoVida = VIDA.COMPLETO;
        }else if(this.vida <= 84 && this.vida >=40){
            estadoVida = VIDA.MITAD;
        }else if(this.vida <= 40 && this.vida > 0){
            estadoVida = VIDA.BAJA;
        }else if(this.vida == 0){
            estadoVida = VIDA.MUERTO;
            System.out.println("MUERTO");
        }
    }

    public double getVida() {
        return vida;
    }

    public void setVida(double vida) {
        this.vida = vida;
    }

    public VIDA getEstadoVida() {
        return estadoVida;
    }

    public void mover(float delta){
        if(estadoVida== VIDA.COMPLETO) {
            int rn = new Random().nextInt(40);
            if (rn == 10) {
                float distanciaY = VELOCIDAD_EN_Y;
                float distanciaX = VELOCIDAD_EN_X;
                if (movDerPrim) {
                    sprite.setX(sprite.getX() + distanciaX / 2);

                    movDerPrim = false;
                    movDerSeg = true;
                } else if (movDerSeg) {
                    sprite.setX(sprite.getX() + distanciaX / 2);

                    movDerSeg = false;
                    movAbaPrim = true;
                } else if (movAbaPrim) {
                    sprite.setY(sprite.getY() - distanciaY / 2);

                    movAbaPrim = false;
                    movAbaSeg = true;
                } else if (movAbaSeg) {
                    sprite.setY(sprite.getY() - distanciaY / 2);

                    movAbaSeg = false;
                    movDiagPrim = true;
                } else if (movDiagPrim) {
                    sprite.setX(sprite.getX() - distanciaX / 2);
                    sprite.setY(sprite.getY() + distanciaY / 2);

                    movDiagPrim = false;
                    movDiagSeg = true;
                } else if (movDiagSeg) {
                    sprite.setX(sprite.getX() - distanciaX / 2);
                    sprite.setY(sprite.getY() + distanciaY / 2);

                    movDiagSeg = false;
                    movDerPrim = true;
                }
            }
        }else if(estadoVida == VIDA.MITAD){
            sprite.setX(Pantalla.ANCHO - 300);
            sprite.setY(100);
        }else if(estadoVida == VIDA.BAJA){
            sprite.setX(Pantalla.ANCHO - 350);
            sprite.setY(Pantalla.ALTO - 320);
        }
    }
}
