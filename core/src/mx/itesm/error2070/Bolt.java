package mx.itesm.error2070;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Daniel Alillo on 20/11/2017.
 */


public class Bolt extends Objeto
{
    private final float VELOCIDAD_X = -700;      // Velocidad horizontal (a la derecha)
    private Animation<TextureRegion> spriteAnimado;         //
    private float timerAnimacion;

    // Recibe la imagen
    public Bolt(Texture textura, float x, float y)
    {
        TextureRegion texturaCompleta = new TextureRegion(textura);
        TextureRegion[][] texturaBala = texturaCompleta.split(175,128);

        spriteAnimado = new Animation(0.07f, texturaBala[0][0], texturaBala[0][1], texturaBala[0][2],texturaBala[0][3],
                texturaBala[0][1], texturaBala[0][4],texturaBala[0][5]);
        // Animación infinita
        spriteAnimado.setPlayMode(Animation.PlayMode.LOOP);
        // Inicia el timer que contará tiempo para saber qué frame se dibuja
        timerAnimacion = 0;


        sprite = new Sprite(texturaBala[0][0]);
        sprite.setPosition(x,y);
    }

    public void dibujar(SpriteBatch batch, EstadoJuego estadoJuego) {
        if (estadoJuego == EstadoJuego.JUGANDO) {

            // Frame que se dibujará
            timerAnimacion += Gdx.graphics.getDeltaTime();
            TextureRegion region = spriteAnimado.getKeyFrame(timerAnimacion);

            batch.draw(region, sprite.getX(), sprite.getY());

        }
        else
        {
            sprite.draw(batch);
        }
    }

    // Mueve el personaje a la derecha
    public void mover(float delta) {
        float distancia = VELOCIDAD_X*delta;
        sprite.setX(sprite.getX()+distancia);
    }

    public boolean chocaCon(RobotRunner robotRunner) {
        return sprite.getBoundingRectangle().overlaps(robotRunner.sprite.getBoundingRectangle());
    }

    public boolean chocaCon(Escudo escudo) {
        return sprite.getBoundingRectangle().overlaps(escudo.sprite.getBoundingRectangle());
    }
}

