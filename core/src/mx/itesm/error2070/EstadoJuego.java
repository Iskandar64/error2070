package mx.itesm.error2070;

/**
 * Created by roberto on 16/03/17.
 */

public enum EstadoJuego
{
    JUGANDO,
    PAUSADO,
    PIERDE,
}
