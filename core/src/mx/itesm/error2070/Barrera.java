package mx.itesm.error2070;

import com.badlogic.gdx.graphics.Texture;

/**
 * Created by Aldo on 19/11/2017.
 */

public class Barrera extends Objeto {

    private Doom doom;

    public Barrera(Texture textura, Doom doom){
        super(textura, Pantalla.ANCHO - 450, 75);//Recibe imagen
        this.doom = doom;
    }

    public boolean chocaCon(Bala bala){
        return sprite.getBoundingRectangle().overlaps(bala.sprite.getBoundingRectangle());
    }

    public boolean chocaCon(Granada granada){
        return sprite.getBoundingRectangle().overlaps(granada.sprite.getBoundingRectangle());
    }

}
