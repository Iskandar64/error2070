package mx.itesm.error2070;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Muestra un texto en la pantalla.
 */

public class Texto
{
    private BitmapFont font;
    private boolean color;

    public Texto(String archivo, boolean color)
    {
        font = new BitmapFont(Gdx.files.internal(archivo));
        this.color = color;

    }

    public void mostrarMensaje(SpriteBatch batch, String mensaje, float x, float y) {
        GlyphLayout glyp = new GlyphLayout();
        glyp.setText(font, mensaje);
        float anchoTexto = glyp.width;

        if(color)
        {
            font.setColor(Color.BLUE);
        }
        font.draw(batch, glyp, x-anchoTexto/2, y);

    }
}
