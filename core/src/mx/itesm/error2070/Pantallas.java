package mx.itesm.error2070;

/**
 * Created by roberto on 13/03/17.
 */

public enum Pantallas
{
    MENU,
    WINNER2,
    WINNER3,
    NIVEL_COLLECT,
    NIVEL_DOOM,
    NIVEL_RUNNER,
    INSTRUCCIONES
}
