package mx.itesm.error2070;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;

import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;

import com.badlogic.gdx.math.Vector3;



/**
 * Created by Daniel Alillo on 30/10/2017.
 */

public class Ganador extends Pantalla

{
    private final Error game;
    private static final float WORDL_WIDTH = 1280;
    private static final float WORDL_HEIGHT = 720;



    private Texture backgroundTexture;
    private Objeto background;

    private Texto scoreMes;
    private int score;
    private Texto mensaje;
    private String logro ="";




    public Ganador(Error game)
    {
        this.game = game;
    }


    @Override
    public void show() {


        Gdx.input.setInputProcessor(new ProcesadorEntrada());

        score = (int) game.scoreFinal;
        scoreMes = new Texto("ethno.fnt", false);
        mensaje = new Texto("ethno.fnt", false);

        backgroundTexture = new Texture(Gdx.files.internal("Fondos/victoria.jpg"));//nombres de los integrantes
        background = new Objeto(backgroundTexture, 10,0);

        if(score >= 5500 && game.lastPlayed == 1)
        {
            logro = "DESBLOQUEASTE: \n Menos energia gastada para \n disparo y escudo";
        }
        else if(score >= 9500 && game.redSkin>4 &&game.lastPlayed == 2)
        {
            logro = "DESBLOQUEASTE: \n Menos danio recibido";
        }

    }

    private class ProcesadorEntrada implements InputProcessor
    {
        private Vector3 v = new Vector3();



        @Override
        public boolean touchDown(int screenX, int screenY, int pointer, int button) {
            v.set(screenX, screenY, 0);
            camara.unproject(v);
            game.setScreen(new SeleccionaNivel(game));
            dispose();

            return true;
        }

        @Override
        public boolean touchUp(int screenX, int screenY, int pointer, int button) {

            return true;
        }

        @Override
        public boolean touchDragged(int screenX, int screenY, int pointer) {
            return true;
        }

        @Override
        public boolean mouseMoved(int screenX, int screenY) {
            return false;
        }

        @Override
        public boolean scrolled(int amount) {
            return false;
        }

        @Override
        public boolean keyDown(int keycode) {
            return false;
        }

        @Override
        public boolean keyUp(int keycode) {
            return false;
        }

        @Override
        public boolean keyTyped(char character) {
            return false;
        }
    }

    @Override
    public void render(float delta)
    {
        camara.update();
        borrarPantalla();
        batch.setProjectionMatrix(camara.combined);

        batch.begin();
        background.dibujar(batch);

        scoreMes.mostrarMensaje(batch, "Score: "+score,325, 375);
        mensaje.mostrarMensaje(batch, logro, 700, 250);
        batch.end();
        //clearScreen();


    }

    @Override
    public void resize(int width, int height)
    {
        super.resize(width, height);


    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }


    private void clearScreen()
    {
        Gdx.gl.glClearColor((float)0, (float)0, 0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    public void dispose()
    {

        backgroundTexture.dispose();


    }

}
