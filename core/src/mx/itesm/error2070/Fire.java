package mx.itesm.error2070;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Daniel Alillo on 14/11/2017.
 */

public class Fire extends Objeto {
    private final float VELOCIDAD_X = -350;      // Velocidad horizontal (a la izquierda)

    private Animation<TextureRegion> spriteAnimado;
    private float timerAnimacion;

    public Fire(Texture textura,  float x, float y) {
        // Lee la textura como región
        TextureRegion texturaCompleta = new TextureRegion(textura);

        TextureRegion[][] texturaFuego = texturaCompleta.split(333,423);

        spriteAnimado = new Animation(0.06f, texturaFuego[0][0], texturaFuego[0][1],
                texturaFuego[0][2],texturaFuego[0][3],texturaFuego[0][4],texturaFuego[0][5]);
        // Animación infinita
        spriteAnimado.setPlayMode(Animation.PlayMode.LOOP);
        // Inicia el timer que contará tiempo para saber qué frame se dibuja
        timerAnimacion = 0;

        sprite = new Sprite(texturaFuego[0][0]);
        sprite.setPosition(x,y);
    }

    public void dibujar(SpriteBatch batch, EstadoJuego estadoJuego) {

        if (estadoJuego == EstadoJuego.JUGANDO) {
            timerAnimacion += Gdx.graphics.getDeltaTime();
            // Frame que se dibujará
            TextureRegion region = spriteAnimado.getKeyFrame(timerAnimacion);
            batch.draw(region, sprite.getX(), sprite.getY());
        }
        else
            {
            sprite.draw(batch);
        }
    }

        public void mover(float delta)
    {
        float distancia = VELOCIDAD_X*delta;
        sprite.setX(sprite.getX()+distancia);
    }

    public void setX(float xx)
    {
        sprite.setX(xx);
    }


}
