package mx.itesm.error2070;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Proyectiles que lanza el robot
 */

public class Bala extends Objeto
{
    private final float VELOCIDAD= 1000;
    private  float trayectoriaEnX ;
    private  float trayectoriaEnY ;
    private  float longitud ;
    private  float vecX;
    private  float vecY ;
    private  float errorRadianesY ;
    private  float angulo ;
    private  float rotacion ;

    private Animation<TextureRegion> spriteAnimado;         //
    private float timerAnimacion;

    private Animation<TextureRegion> spriteRojo;         //




    public Bala(Texture textura, Texture roja, float x, float y)
    {
        // Lee la textura como región
        TextureRegion texturaCompleta = new TextureRegion(textura);
        TextureRegion[][] texturaBala = texturaCompleta.split(90,38);

        TextureRegion textRojo = new TextureRegion(roja);
        TextureRegion[][] texturaRoja = textRojo.split(90,45);

        spriteAnimado = new Animation(0.5f, texturaBala[0][0], texturaBala[0][1], texturaBala[0][2]);
        spriteRojo  = new Animation(0.5f, texturaRoja[0][0], texturaRoja[0][1], texturaRoja[0][2]);
        // Animación infinita
        spriteAnimado.setPlayMode(Animation.PlayMode.LOOP);
        spriteRojo.setPlayMode(Animation.PlayMode.LOOP);
        // Inicia el timer que contará tiempo para saber qué frame se dibuja
        timerAnimacion = 0;



        sprite = new Sprite(texturaBala[0][0]);
        sprite.setPosition(x,y);



        trayectoriaEnX= Gdx.input.getX() - sprite.getX();
        trayectoriaEnY = Gdx.input.getY() - sprite.getY();
          longitud = (float)Math.sqrt(trayectoriaEnX*trayectoriaEnX + trayectoriaEnY*trayectoriaEnY);
          vecX = trayectoriaEnX/longitud;
          vecY = trayectoriaEnY/longitud;
          errorRadianesY = (Gdx.input.getY()<Pantalla.ALTO/2)?(float)Math.toRadians(45):(float)Math.toRadians(60);
         angulo = (float)Math.atan2(vecX,vecY) - errorRadianesY;
          rotacion = (float)Math.toDegrees(angulo);

    }


    public void dibujar(SpriteBatch batch, EstadoJuego estadoJuego, boolean titan)
    {
        if(estadoJuego == EstadoJuego.JUGANDO)
        {
            timerAnimacion += Gdx.graphics.getDeltaTime();
            if(titan)
            {
                // Frame que se dibujará
                TextureRegion region = spriteRojo.getKeyFrame(timerAnimacion);


                batch.draw(region, sprite.getX(), sprite.getY(), sprite.getWidth()/2,sprite.getHeight()/2,
                        sprite.getWidth(),sprite.getHeight(),1,1,rotacion);
            }
            else
            {
                // Frame que se dibujará
                TextureRegion region = spriteAnimado.getKeyFrame(timerAnimacion);


                batch.draw(region, sprite.getX(), sprite.getY(), sprite.getWidth()/2,sprite.getHeight()/2,
                        sprite.getWidth(),sprite.getHeight(),1,1,rotacion);
            }


        }
        else
        {
            if(titan)
            {
                sprite.setColor(Color.RED);
            }
            sprite.draw(batch);
        }
    }

    public void mover(float delta) {
        sprite.setRotation(rotacion);
        sprite.setX(sprite.getX() + (float) Math.cos(angulo) * VELOCIDAD * delta);//Movimiento en X
        sprite.setY(sprite.getY() + (float) Math.sin(angulo) * VELOCIDAD * delta);//Movimiento en Y
    }

    public boolean chocaCon(Gorath gorath) {
        return sprite.getBoundingRectangle().overlaps(gorath.sprite.getBoundingRectangle());
    }
    public boolean chocaCon(Nave nave) {
        return sprite.getBoundingRectangle().overlaps(nave.sprite.getBoundingRectangle());
    }

    public boolean chocaCon(Barrera escudoDoom){
        return sprite.getBoundingRectangle().overlaps(escudoDoom.sprite.getBoundingRectangle());
    }
    public boolean chocaCon(Doom doom)
    {
        return sprite.getBoundingRectangle().overlaps(doom.sprite.getBoundingRectangle());
    }
}
