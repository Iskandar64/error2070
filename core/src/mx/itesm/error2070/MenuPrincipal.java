package mx.itesm.error2070;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.FillViewport;

/**
 * Created by Daniel Alillo on 12/10/2017.
 */
/* Menu principal. solo tiene boton play y creditos*/

public class MenuPrincipal extends ScreenAdapter
{
    private final Error game;
    private static final float WORDL_WIDTH = 1280;
    private static final float WORDL_HEIGHT = 720;

    private Texture play;//para empezar a jugar
    private Table table;//tabla para colocar los botones
    private Texture pressPlayTexture;//cuando se presiona el boton play
    private Texture credit;//boton que lleva a los creditos
    private Texture creditPress;//cuando se presione el botón credit
    private Texture instrucciones;
    private Texture instruccionesPress;

    private ImageButton plBtn;
    private ImageButton insBtn;
    private ImageButton creBtn;


    private Stage stage;
    private AssetManager manager;

    private Texture backgroundTexture;

    public MenuPrincipal(Error game)
    {
        this.game = game;
        manager = this.game.getAssetManager();
    }

    @Override
    public void show()
    {
        super.show();
        game.music = manager.get("sounds/revelation.mp3");
        game.music.setVolume(0.5f);
        game.music.setLooping(true);
        game.music.play();

        game.mennus = manager.get("sounds/menu.mp3");
        stage = new Stage(new FillViewport(WORDL_WIDTH, WORDL_HEIGHT));
        Gdx.input.setInputProcessor(stage);

        play = new Texture(Gdx.files.internal("botones/campanaLogo.png"));
        pressPlayTexture =  new Texture(Gdx.files.internal("botones/campanaLogo1.png"));

         backgroundTexture = new Texture(Gdx.files.internal("Fondos/titulo.jpg"));
        Image background = new Image((backgroundTexture));
        stage.addActor(background);

         plBtn = new ImageButton(new TextureRegionDrawable(new TextureRegion(play)),
                new TextureRegionDrawable((new TextureRegion(pressPlayTexture))));
        plBtn.addListener(new ActorGestureListener() {

            public void tap(InputEvent event, float x, float y, int count, int button)
            {
                super.tap( event,  x,  y,  count, button);
                game.mennus.play();
                game.setScreen(new SeleccionaNivel(game));

                dispose();
            }
        });
        plBtn.setPosition(100, 50);


        credit = new Texture(Gdx.files.internal("botones/creditos.png"));
        creditPress =  new Texture(Gdx.files.internal("botones/creditos1.png"));

        creBtn = new ImageButton(new TextureRegionDrawable(new TextureRegion(credit)),
                new TextureRegionDrawable((new TextureRegion(creditPress))));
        creBtn.addListener(new ActorGestureListener() {

            public void tap(InputEvent event, float x, float y, int count, int button)
            {
                super.tap( event,  x,  y,  count, button);
                game.mennus.play();
                game.setScreen(new Creditos(game));
                dispose();
            }
        });
        creBtn.setPosition(450, 50);


        instrucciones = new Texture(Gdx.files.internal("botones/instruccionesLogo.png"));
        instruccionesPress =  new Texture(Gdx.files.internal("botones/instruccionesLogo1.png"));

        insBtn = new ImageButton(new TextureRegionDrawable(new TextureRegion(instrucciones)),
                new TextureRegionDrawable((new TextureRegion(instruccionesPress))));
        insBtn.addListener(new ActorGestureListener() {

            public void tap(InputEvent event, float x, float y, int count, int button)
            {
                super.tap( event,  x,  y,  count, button);
                game.mennus.play();
                game.setScreen(new PantallaCargando(game, Pantallas.INSTRUCCIONES));
                dispose();
            }
        });
        insBtn.setPosition(800, 40);



        stage.addActor(plBtn);
        stage.addActor(creBtn);
        stage.addActor(insBtn);


    }

    @Override
    public void render(float delta)
    {
        super.render(delta);
        clearScreen();
        stage.act(delta);
        stage.draw();

    }

    @Override
    public void resize(int width, int height)
    {
        super.resize(width, height);
        stage.getViewport().update(width, height);

    }


    private void clearScreen()
    {
        Gdx.gl.glClearColor((float)0, (float)0, 0,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    public void dispose()
    {
        super.dispose();;
        stage.dispose();
        play.dispose();
        pressPlayTexture.dispose();
        credit.dispose();
        creditPress.dispose();
        instruccionesPress.dispose();
        instrucciones.dispose();

    }

}
