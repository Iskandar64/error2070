package mx.itesm.error2070;

import com.badlogic.gdx.graphics.Texture;

/**
 * Created by Aldo on 20/11/2017.
 */

public class BulletDiag extends Objeto {

    private final float VELOCIDAD = -1200;
    private final float angulo = (float)Math.toRadians(20);
    private final float rotacion = (float)Math.toDegrees(angulo);

    public BulletDiag(Texture textura, float x, float y){super(textura, x, y);}

    public void mover(float delta){
        sprite.setRotation(rotacion);
        sprite.setX(sprite.getX() + (float) Math.cos(angulo) * VELOCIDAD * delta);//Movimiento en X
        sprite.setY(sprite.getY() + (float) Math.sin(angulo) * VELOCIDAD * delta);//Movimiento en Y
    }

    public boolean chocaCon(RobotRunner robotRunner){
        return sprite.getBoundingRectangle().overlaps(robotRunner.sprite.getBoundingRectangle());
    }

    public boolean chocaCon(Escudo escudo){
        return sprite.getBoundingRectangle().overlaps(escudo.sprite.getBoundingRectangle());
    }

}
